﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerControllerManager : MonoBehaviour
{
    public GameObject cameraObj;
    public LayerMask mask;
    public float cameraSpeed;
    public MapManager mapManager;
    public Button[] touchingModeButtons;

    Camera cameraComp;
    Vector2 prevPositionFinger0;
    float prevDistanceBetweenFingers;
    bool isZooming;
    bool isMoving;
    bool hasUIBeenClicked;

    void Start()
    {
        cameraComp = cameraObj.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0)
        {
            isZooming = false;
            isMoving = false;
            hasUIBeenClicked = false;
        }

        if (Input.touchCount == 1)
        {
            if (!isZooming)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Moved)
                {
                    isMoving = true;
                    MoveCamera();
                }
                else if (touch.phase == TouchPhase.Began && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                {
                    hasUIBeenClicked = true;
                }
                else if (touch.phase == TouchPhase.Ended && !hasUIBeenClicked && !isMoving)
                {
                    TouchObject(touch);
                }
                prevPositionFinger0 = GetPercentScreenPositionOfFinger(0);
            }
        }
        else if (Input.touchCount == 2)
        {
            if (Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                isZooming = true;
                ZoomCamera();
            }
            prevPositionFinger0 = GetPercentScreenPositionOfFinger(0);
            prevDistanceBetweenFingers = (prevPositionFinger0 - GetPercentScreenPositionOfFinger(1)).magnitude;
        }
    }

    Vector2 GetPercentScreenPositionOfFinger(int FingerIndex)
    {
        Vector2 pos = Input.GetTouch(FingerIndex).position;
        return new Vector2(pos.x / Screen.width, pos.y / Screen.height);
    }

    void MoveCamera()
    {
        Vector2 NewPosition = GetPercentScreenPositionOfFinger(0);
        Vector2 PositionDifference = NewPosition - prevPositionFinger0;
        cameraObj.transform.Translate(new Vector3(-PositionDifference.x, .0f, -PositionDifference.y) * cameraSpeed, Space.World);
    }

    void ZoomCamera()
    {
        float newDistance = (GetPercentScreenPositionOfFinger(0) - GetPercentScreenPositionOfFinger(1)).magnitude;
        float delta = newDistance - prevDistanceBetweenFingers;
        cameraObj.transform.Translate(new Vector3(.0f, .0f, delta) * cameraSpeed * 3);
    }

    void TouchObject(Touch touch)
    {
        Ray ray = cameraComp.ScreenPointToRay(touch.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask.value))
        {
            GameObject recipient = hit.transform.gameObject;
            recipient.SendMessage("Touch", hit.point, SendMessageOptions.DontRequireReceiver);
        }
    }

    public void SetMode(bool isActive, MapManager.Mode mode)
    {
        if (isActive)
        {
            mapManager.mode = mode;
            if (mode != MapManager.Mode.Path)
            {
                mapManager.ClearPath();
            }
        }
        else
        {
            mapManager.mode = MapManager.Mode.None;
            mapManager.ClearPath();
        }
    }

    public void UpdateRoofs()
    {
        mapManager.UpdateRoofs();
    }

    public void DeactivateOtherButtons(Button button)
    {
        foreach (Button b in touchingModeButtons)
        {
            if (b != button)
            {
                b.GetComponent<SwitchingModeButtonHandler>().setActive(false);
            }
        }
    }
}
