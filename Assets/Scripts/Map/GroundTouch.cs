﻿using UnityEngine;

public class GroundTouch : MonoBehaviour
{
    private MapManager manager;
    private int x, z;

    public void Touch(Vector3 point)
    {
        manager.TouchGround(x, z);
    }

    public void Init(MapManager manager, int x, int z)
    {
        this.manager = manager;
        this.x = x;
        this.z = z;
    }
}
