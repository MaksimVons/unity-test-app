﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTouch : MonoBehaviour
{
    private MapManager manager;
    private int x, z, side;

    public void Init(MapManager manager, int x, int z, int side)
    {
        this.x = x;
        this.z = z;
        this.side = side;
        this.manager = manager;
    }

    public void Touch(Vector3 point)
    {
        manager.TouchWall(x, z, side);
    }
}
