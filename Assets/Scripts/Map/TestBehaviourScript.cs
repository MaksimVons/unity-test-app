﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBehaviourScript : MonoBehaviour
{
    private int i = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (i++ % 60 == 0)
        {
            GetComponent<Renderer>().material.color = Random.ColorHSV();
        }
    }
}
