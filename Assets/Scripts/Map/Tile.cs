﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public int X { get; }
    public int Z { get; }
    public GameObject ground;
    public GameObject floor;
    public GameObject[] Walls { get; }
    public GameObject roof;
    public bool CanBuild { get; }

    public Tile(GameObject ground, bool canBuild, int x, int z)
    {
        this.ground = ground;
        CanBuild = canBuild;
        X = x;
        Z = z;
        Walls = new GameObject[4];
    }

}
