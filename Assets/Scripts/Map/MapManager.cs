﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public class MapManager : MonoBehaviour
{
    public enum Mode { None, Floors, Walls, Path };

    public int columns;
    public int rows;

    public GameObject groundPrefab;
    public GameObject floorPrefab;
    public GameObject wallPrefab;
    public GameObject roofPrefab;
    public Mode mode;

    private Tile tileA, tileB;
    private List<Tile> path;

    private GameObject mapRoot;
    private Tile[,] tiles;

    private static readonly float TileSize = 5f;
    private static readonly Vector3 FloorTranslation = new Vector3(.0f, .125f, .0f);
    private static readonly Vector3 RoofTranslation = new Vector3(.0f, 3.87f, .0f);
    private static readonly Vector3[] WallsTranslation =
        {
        new Vector3(.0f, 1.9325f, 2.5f),
        new Vector3(2.5f, 1.9325f, .0f),
        new Vector3(.0f, 1.9325f, -2.5f),
        new Vector3(-2.5f, 1.9325f, .0f)
    };

    void Start()
    {
        CreateMap();
    }

    public void CreateMap()
    {
        mapRoot = new GameObject("Map");

        tiles = new Tile[columns, rows];

        for (int x = 0; x < columns; x++)
        {
            for (int z = 0; z < rows; z++)
            {
                GameObject ground = Instantiate(groundPrefab, new Vector3(TileSize * x, 0f, TileSize * z), Quaternion.identity, mapRoot.transform);
                ground.name = "Ground_" + x + "_" + z;
                ground.GetComponent<GroundTouch>().Init(this, x, z);

                Tile tile = new Tile(ground, !((x == 0) || (x == columns - 1) || (z == 0) || (z == rows - 1)), x, z);
                tiles[x, z] = tile;
            }
        }
    }

    public void TouchFloor(int x, int z, Vector3 touchLocalPos)
    {
        switch (mode)
        {
            case Mode.Floors:
                RemoveFloor(x, z);
                break;
            case Mode.Walls:
                int side;
                if (Mathf.Abs(touchLocalPos.x) > Mathf.Abs(touchLocalPos.z))
                {
                    if (touchLocalPos.x > 0)
                    {
                        side = 1;
                    }
                    else
                    {
                        side = 3;
                    }
                }
                else
                {
                    if (touchLocalPos.z > 0)
                    {
                        side = 0;
                    }
                    else
                    {
                        side = 2;
                    }
                }
                BuildWall(x, z, side);
                break;
            case Mode.Path:
                SelectNextTileForPath(tiles[x, z]);
                break;
        }
    }

    public void TouchGround(int x, int z)
    {
        switch (mode)
        {
            case Mode.Floors:
                BuildFloor(x, z);
                break;
            case Mode.Path:
                SelectNextTileForPath(tiles[x, z]);
                break;
        }
    }

    public void TouchWall(int x, int z, int side)
    {
        if (mode == Mode.Walls)
        {
            RemoveWall(x, z, side);
        }
    }

    public void UpdateRoofs()
    {
        var notUpdatedTiles = new HashSet<Tile>();
        foreach (Tile t in tiles)
        {
            notUpdatedTiles.Add(t);
        }

        while (notUpdatedTiles.Count > 0)
        {
            var ie = notUpdatedTiles.GetEnumerator();
            ie.MoveNext();
            Tile tile = ie.Current;

            var tilesInArea = new List<Tile>();
            Boolean isOpenArea = false;

            BFS(tile,
                (Tile t, Tile nearTile, int side) =>
                (t.Walls[side] == null),
                t =>
                {
                    if (t.X == 0 || t.Z == 0 || t.floor == null)
                    {
                        isOpenArea = true;
                    }
                    tilesInArea.Add(t);
                }
            );

            foreach (Tile t in tilesInArea)
            {
                if (isOpenArea && t.roof != null)
                {
                    RemoveRoof(t.X, t.Z);
                }
                else if (!isOpenArea && t.roof == null)
                {
                    BuildRoof(t.X, t.Z);
                }
                notUpdatedTiles.Remove(t);
            }
        }
    }

    public void ClearPath()
    {
        ClearTileHighlight(tileA);
        ClearTileHighlight(tileB);

        tileA = null;
        tileB = null;

        if (path != null)
        {
            foreach (Tile tile in path)
            {
                ClearTileHighlight(tile);
            }
            path.Clear();
        }
    }

    private void SelectNextTileForPath(Tile tile)
    {
        if (tileA != null)
        {
            if (tileB != null)
            {
                ClearPath();
            }
            else
            {
                tileB = tile;
                HighlightTile(tileB, Color.blue);
                HighlightPath();
                return;
            }
        }
        tileA = tile;
        HighlightTile(tileA, Color.blue);
    }

    private void HighlightPath()
    {
        if (tileA != null && tileB != null && tileA != tileB)
        {
            path = GetPath(tileA, tileB);

            if (path != null)
            {
                foreach (Tile t in path)
                {
                    HighlightTile(t, Color.green);
                }
            }
            else
            {
                HighlightTile(tileB, Color.red);
            }
        }
    }


    private void HighlightTile(Tile tile, Color color)
    {
        if (tile != null)
        {
            if (tile.floor != null)
            {
                tile.floor.GetComponent<Renderer>().material.color = color;
            }
            else
            {
                tile.ground.GetComponent<Renderer>().material.color = color;
            }
        }
    }

    private void ClearTileHighlight(Tile tile)
    {
        if (tile != null)
        {
            if (tile.floor != null)
            {
                tile.floor.GetComponent<Renderer>().material.color = Color.white;
            }
            else
            {
                tile.ground.GetComponent<Renderer>().material.color = Color.white;
            }
        }
    }

    private void BuildRoof(int x, int z)
    {
        Tile tile = tiles[x, z];
        if (tile.roof == null)
        {
            GameObject roof = Instantiate(roofPrefab, tile.ground.transform.position, tile.ground.transform.rotation, mapRoot.transform);
            roof.name = "Roof_" + x + "_" + z;

            roof.transform.Translate(RoofTranslation);

            tile.roof = roof;
        }
    }

    private void RemoveRoof(int x, int z)
    {
        Tile tile = tiles[x, z];
        if (tile.roof != null)
        {
            Object.Destroy(tile.roof);
        }
    }

    private void BuildFloor(int x, int z)
    {
        Tile tile = tiles[x, z];
        if (tile.CanBuild && tile.floor == null)
        {
            GameObject floor = Instantiate(floorPrefab, tile.ground.transform.position, tile.ground.transform.rotation, mapRoot.transform);
            floor.name = "Floor_" + x + "_" + z;
            floor.GetComponent<FloorTouch>().Init(this, x, z);

            floor.transform.Translate(FloorTranslation);

            tile.floor = floor;
        }

    }

    private void RemoveFloor(int x, int z)
    {
        Tile tile = tiles[x, z];
        if (tile.CanBuild && tile.floor != null)
        {
            // removing walls that have only this floor under them
            for (int side = 0; side < 4; side++)
            {
                if (tile.Walls[side] != null)
                {
                    Tile nearTile = GetNearTile(x, z, side);
                    if (nearTile != null && nearTile.floor == null)
                    {
                        Object.Destroy(tile.Walls[side]);
                    }
                }
            }
        }
        Object.Destroy(tile.floor);
    }


    /// <summary>
    /// Builds wall (depends on buildingMode)
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <param name="side">0 - up, 1 - right, 2 - down, 3 - left</param>
    private void BuildWall(int x, int z, int side)
    {
        Tile tile = tiles[x, z];
        if (tile.CanBuild && tile.floor != null && tile.Walls[side] == null)
        {
            GameObject wall = Instantiate(wallPrefab, tile.ground.transform.position, tile.ground.transform.rotation, mapRoot.transform);
            wall.name = "Wall_" + x + "_" + z + "_" + side;
            wall.GetComponent<WallTouch>().Init(this, x, z, side);

            wall.transform.Translate(WallsTranslation[side]);
            if (side % 2 != 0)
            {
                wall.transform.Rotate(.0f, 90f, .0f);
            }

            tile.Walls[side] = wall;
            int otherTileSide = (side > 1) ? (side - 2) : (side + 2);
            GetNearTile(x, z, side).Walls[otherTileSide] = wall;
        }
    }

    private void RemoveWall(int x, int z, int side)
    {
        Tile tile = tiles[x, z];
        if (tile.CanBuild && tile.Walls[side] != null)
        {
            Object.Destroy(tile.Walls[side]);

            UpdateRoofsAfterWallRemoving(x, z, side);
        }

    }

    private void UpdateRoofsAfterWallRemoving(int x, int z, int side)
    {
        Tile nearTile = GetNearTile(x, z, side);
        if (nearTile != null && ((nearTile.roof == null) != (tiles[x, z].roof == null)))
        {
            if (nearTile.roof != null)
            {
                RemoveRoofsInThisRoom(nearTile);
            }
            else
            {
                RemoveRoofsInThisRoom(tiles[x, z]);
            }
        }
    }

    private void RemoveRoofsInThisRoom(Tile firstTile)
    {
        BFS(firstTile,
            (Tile tile, Tile nearTile, int side) =>
            (tile.Walls[side] == null),
            t => Object.Destroy(t.roof));
    }

    private void BFS(Tile firstTile, Func<Tile, Tile, int, bool> edgeCheck, Action<Tile> action)
    {
        var tilesToView = new Queue<Tile>();
        var viewedTiles = new HashSet<Tile>();

        tilesToView.Enqueue(firstTile);
        viewedTiles.Add(firstTile);

        while (tilesToView.Count != 0)
        {
            Tile tile = tilesToView.Dequeue();

            for (int side = 0; side < 4; side++)
            {
                Tile nearTile = GetNearTile(tile.X, tile.Z, side);
                if (nearTile != null && !viewedTiles.Contains(nearTile) && edgeCheck.Invoke(tile, nearTile, side))
                {
                    viewedTiles.Add(nearTile);
                    tilesToView.Enqueue(nearTile);
                }
            }

            action.Invoke(tile);
        }
    }

    private List<Tile> GetPath(Tile a, Tile b)
    {
        if (a != null && b != null && a != b)
        {
            var viewedTiles = new HashSet<Tile>();
            var tilesToView = new Collection<Tile>();
            var f = new Dictionary<Tile, int>();
            var distance = new Dictionary<Tile, int>();
            var parent = new Dictionary<Tile, Tile>();

            tilesToView.Add(a);
            distance.Add(a, 0);
            f.Add(a, GetHeuristicPathLength(a, b));

            while (tilesToView.Count > 0)
            {
                Tile cur = tilesToView.OrderBy((t) => (f[t])).First();

                if (cur == b)
                {
                    var path = new List<Tile>();
                    for (Tile t = parent[b]; t != a; t = parent[t])
                    {
                        path.Add(t);
                    }
                    path.Reverse();
                    return path;
                }

                tilesToView.Remove(cur);
                viewedTiles.Add(cur);

                for (int side = 0; side < 4; side++)
                {
                    Tile nearTile = GetNearTile(cur.X, cur.Z, side);
                    if (cur.Walls[side] == null && nearTile != null)
                    {
                        int score = distance[cur] + 1;
                        if (viewedTiles.Contains(nearTile) && score >= distance[nearTile])
                        {
                            continue;
                        }
                        parent[nearTile] = cur;
                        distance[nearTile] = score;
                        f[nearTile] = score + GetHeuristicPathLength(nearTile, b);
                        if (!tilesToView.Contains(nearTile))
                        {
                            tilesToView.Add(nearTile);
                        }
                    }
                }
            }
        }
        return null;
    }

    private int GetHeuristicPathLength(Tile a, Tile b)
    {
        return Math.Abs(a.X - b.X) + Math.Abs(a.Z - b.Z);
    }

    private Tile GetNearTile(int x, int z, int side)
    {
        if (side % 2 == 0)
        {
            if (side == 0 && z < rows - 1)
            {
                return tiles[x, z + 1];
            }
            else if (z > 0)
            {
                return tiles[x, z - 1];
            }
        }
        else
        {
            if (side == 1 && x < columns - 1)
            {
                return tiles[x + 1, z];
            }
            else if (x > 0)
            {
                return tiles[x - 1, z];
            }
        }
        return null;
    }
}
