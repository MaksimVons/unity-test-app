﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTouch : MonoBehaviour
{
    private MapManager manager;
    private int x, z;

    public void Init(MapManager manager, int x, int z)
    {
        this.x = x;
        this.z = z;
        this.manager = manager;
    }

    public void Touch(Vector3 point)
    {
        manager.TouchFloor(x, z, point - transform.position);
    }
}
