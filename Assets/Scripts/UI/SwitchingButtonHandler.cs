﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class SwitchingModeButtonHandler : MonoBehaviour
{
    public PlayerControllerManager playerControllerManager;

    public Button button;

    protected bool isActive = false;
    protected MapManager.Mode mode;

    public void setActive(bool isActive)
    {
        this.isActive = isActive;
        if (isActive)
        {
            button.image.color = Color.green;
        }
        else
        {
            button.image.color = Color.white;
        }
    }

    public void OnClick()
    {
        setActive(!isActive);
        playerControllerManager.SetMode(isActive, mode);
        playerControllerManager.DeactivateOtherButtons(button);
    }
}