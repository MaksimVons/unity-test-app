﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateRoofsButton : MonoBehaviour {
    public PlayerControllerManager manager;

    public void OnClick()
    {
        manager.UpdateRoofs();
    }
}
