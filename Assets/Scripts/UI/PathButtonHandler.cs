﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PathButtonHandler : SwitchingModeButtonHandler
{
    public PathButtonHandler()
    {
        mode = MapManager.Mode.Path;
    }
}
