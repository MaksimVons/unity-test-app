﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorsButtonHandler : SwitchingModeButtonHandler
{
    public FloorsButtonHandler()
    {
        mode = MapManager.Mode.Floors;
    }
}
