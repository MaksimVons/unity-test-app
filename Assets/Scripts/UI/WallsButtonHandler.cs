﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallsButtonHandler : SwitchingModeButtonHandler
{
    public WallsButtonHandler()
    {
        mode = MapManager.Mode.Walls;
    }
}
